"use strict";

gotoAndStop(1);
gotoAndPlay(2);

// arrow code for when mouse clicked

onEvent('mouseclick', function() {
    gotoAndPlay(2);
})